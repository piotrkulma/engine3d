package pl.piotrkulma.game3d;

import javax.swing.JFrame;
import pl.piotrkulma.engine3d.Engine3D;

public class Application {
    private Engine3D engine3D;

    public static void main(String[] args) {
        Application application = new Application();
        application.init();
    }

    public void init() {
        initEngine3D();
        initUI();
    }

    private void initEngine3D() {
        engine3D = new Engine3D();
    }

    private void initUI() {
        JFrame frame = new JFrame ("OpenGL");
        frame.getContentPane().add(engine3D.getGlCanvas());
        frame.setSize(frame.getContentPane().getPreferredSize());
        frame.setVisible(true);
    }
}
