package pl.piotrkulma.engine3d;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import pl.piotrkulma.engine3d.input.KeyboardInput;
import pl.piotrkulma.engine3d.input.MouseInput;

public class Engine3D {
    private GLCanvas glCanvas;
    private OpenGLEventDisplay openGLEventDisplay;
    private Cursor defaultCursor;
    private Cursor blankCursor;

    public Engine3D() {
        init();
        initCursors();
    }

    private void init() {
        initGlCanvas();
        initFPSAnimator();
    }

    public GLCanvas getGlCanvas() {
        return glCanvas;
    }

    public void setCursor(boolean visible) {
        if(visible) {
            glCanvas.setCursor(defaultCursor);
        } else {
            glCanvas.setCursor(blankCursor);
        }
    }

    private void initGlCanvas() {
        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);

        MouseInput mouseInput = new MouseInput();
        KeyboardInput keyboardInput = new KeyboardInput();

        openGLEventDisplay = new OpenGLEventDisplay(this, mouseInput, keyboardInput);

        glCanvas = new GLCanvas(capabilities);
        glCanvas.setSize(800, 600);

        glCanvas.addGLEventListener(openGLEventDisplay);
        glCanvas.addKeyListener(keyboardInput);
        glCanvas.addMouseMotionListener(mouseInput);

        glCanvas.setFocusable(true);
    }

    private void initCursors() {
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg, new Point(0, 0), "blank cursor");

        defaultCursor = glCanvas.getCursor();
    }

    private void initFPSAnimator() {
        FPSAnimator animator = new FPSAnimator(glCanvas, 60,true );
        animator.start();
    }
}
