package pl.piotrkulma.engine3d.model;

public class CameraModel {
    private boolean firstMouseUse;

    private float lastX;
    private float lastY;
    private float pitch;
    private float yaw;

    private Point3D cameraPos;
    private Point3D cameraFront;
    private Point3D cameraUp;

    public CameraModel() {
        cameraPos = new Point3D(0, 0, 3);
        cameraFront = new Point3D(0, 0, -1);
        cameraUp = new Point3D(0, 1, 0);
    }

    public boolean isFirstMouseUse() {
        return firstMouseUse;
    }

    public void setFirstMouseUse(boolean firstMouseUse) {
        this.firstMouseUse = firstMouseUse;
    }

    public float getLastX() {
        return lastX;
    }

    public void setLastX(float lastX) {
        this.lastX = lastX;
    }

    public float getLastY() {
        return lastY;
    }

    public void setLastY(float lastY) {
        this.lastY = lastY;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public Point3D getCameraPos() {
        return cameraPos;
    }

    public void setCameraPos(Point3D cameraPos) {
        this.cameraPos = cameraPos;
    }

    public Point3D getCameraFront() {
        return cameraFront;
    }

    public void setCameraFront(Point3D cameraFront) {
        this.cameraFront = cameraFront;
    }

    public Point3D getCameraUp() {
        return cameraUp;
    }

    public void setCameraUp(Point3D cameraUp) {
        this.cameraUp = cameraUp;
    }
}
