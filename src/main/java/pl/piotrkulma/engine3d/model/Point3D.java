package pl.piotrkulma.engine3d.model;

public class Point3D {
    private float x;
    private float y;
    private float z;

    public Point3D(float... values) {
        this.x = values[0];
        this.y = values[1];
        this.z = values[2];
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
