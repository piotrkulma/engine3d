package pl.piotrkulma.engine3d.util;

public enum KeyCode {
    LEFT_KEY(37),
    UP_KEY(38),
    RIGHT_KEY(39),
    DOWN_KEY(40);

    private int keyCode;

    KeyCode(int value) {
        this.keyCode = value;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public static KeyCode valueOf(int code) {
        for(KeyCode kc : KeyCode.values()) {
            if(kc.getKeyCode() == code) {
                return kc;
            }
        }

        return null;
    }
}
