package pl.piotrkulma.engine3d.util;

public final class Util {
    public static boolean isValidKeyCode(int code) {
        for(KeyCode kc : KeyCode.values()) {
            if(kc.getKeyCode() == code) {
                return true;
            }
        }

        return false;
    }

    public static boolean isNotValidKeyCode(int code) {
        return !isValidKeyCode(code);
    }

    private Util(){}
}
