package pl.piotrkulma.engine3d.util;

import com.jogamp.opengl.glu.GLU;
import pl.piotrkulma.engine3d.model.CameraModel;
import pl.piotrkulma.engine3d.model.Point3D;

import static pl.piotrkulma.engine3d.util.MathUtil.*;

public class CameraUtil {
    private static float cameraSpeed = 0.1f;

    public static void moveForward(CameraModel cameraModel) {
        cameraModel.setCameraPos(MathUtil.add(cameraModel.getCameraPos(), mult(cameraModel.getCameraFront(), cameraSpeed)));
    }

    public static void moveBack(CameraModel cameraModel) {
        cameraModel.setCameraPos(MathUtil.sub(cameraModel.getCameraPos(), mult(cameraModel.getCameraFront(), cameraSpeed)));
    }

    public static void moveLeft(CameraModel cameraModel) {
        cameraModel.setCameraPos(
                MathUtil.sub(cameraModel.getCameraPos(),
                        mult(
                                MathUtil.normalize(MathUtil.cross(cameraModel.getCameraFront(), cameraModel.getCameraUp())), cameraSpeed)));
    }

    public static void moveRight(CameraModel cameraModel) {
        cameraModel.setCameraPos(
                MathUtil.add(cameraModel.getCameraPos(),
                        mult(
                                MathUtil.normalize(MathUtil.cross(cameraModel.getCameraFront(), cameraModel.getCameraUp())), cameraSpeed)));
    }

    public static void lookAt(GLU glu, CameraModel cameraModel) {
        Point3D center = MathUtil.add(cameraModel.getCameraPos(), cameraModel.getCameraFront());
        glu.gluLookAt(
                cameraModel.getCameraPos().getX(), cameraModel.getCameraPos().getY(), cameraModel.getCameraPos().getZ(),
                center.getX(), center.getY(), center.getZ(),
                cameraModel.getCameraUp().getX(), cameraModel.getCameraUp().getY(), cameraModel.getCameraUp().getZ());
    }

    public static void mouseMove(CameraModel cameraModel, float x, float y){
        if(cameraModel.isFirstMouseUse()) {
            cameraModel.setFirstMouseUse(false);
            cameraModel.setLastX(x);
            cameraModel.setLastY(y);
        }

        float xOffset = x - cameraModel.getLastX();
        float yOffset = y - cameraModel.getLastY();
        cameraModel.setLastX(x);
        cameraModel.setLastY(y);

        float sensitivity = 1f;
        xOffset *= sensitivity;
        yOffset *= sensitivity;

        cameraModel.setYaw(cameraModel.getYaw() + xOffset);
        cameraModel.setPitch(cameraModel.getPitch() + yOffset);

        if(cameraModel.getPitch() > 89.0f) {
            cameraModel.setPitch(89.0f);
        }else if(cameraModel.getPitch() < -89.0f) {
            cameraModel.setPitch(-89.0f);
        }

        Point3D front = new Point3D(
                (float) (Math.cos(MathUtil.degToRad(cameraModel.getYaw())) * Math.cos(MathUtil.degToRad(cameraModel.getPitch()))),
                cameraModel.getCameraFront().getY(),
                //(float) Math.sin(Basic3DUtil.degToRad(cameraModel.getPitch())),
                (float) (Math.sin(MathUtil.degToRad(cameraModel.getYaw())) * Math.cos(MathUtil.degToRad(cameraModel.getPitch())))
        );

        cameraModel.setCameraFront(MathUtil.normalize(front));
    }
}
