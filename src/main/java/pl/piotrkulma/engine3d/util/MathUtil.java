package pl.piotrkulma.engine3d.util;

import pl.piotrkulma.engine3d.model.Point3D;

public final class MathUtil {
    public static final float[][] QUBE = {
            // Front face
            {-1.0f, -1.0f, 1.0f},
            {1.0f, -1.0f, 1.0f},
            {1.0f, 1.0f, 1.0f},
            {-1.0f, 1.0f, 1.0f},

            // Back face
            {-1.0f, -1.0f, -1.0f},
            {-1.0f, 1.0f, -1.0f},
            {1.0f, 1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},

            // Top face
            {-1.0f, 1.0f, -1.0f},
            {-1.0f, 1.0f, 1.0f},
            {1.0f, 1.0f, 1.0f},
            {1.0f, 1.0f, -1.0f},

            // Bottom face
            {-1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, 1.0f},
            {-1.0f, -1.0f, 1.0f},

            // Right face
            {1.0f, -1.0f, -1.0f},
            {1.0f, 1.0f, -1.0f},
            {1.0f, 1.0f, 1.0f},
            {1.0f, -1.0f, 1.0f},

            // Left face
            {-1.0f, -1.0f, -1.0f},
            {-1.0f, -1.0f, 1.0f},
            {-1.0f, 1.0f, 1.0f},
            {-1.0f, 1.0f, -1.0f}
    };

    public static float degToRad(float deg) {
        return (float) (deg * Math.PI / 180f);
    }

    public static Point3D normalize(Point3D p) {
        float mag = mag(p);

        return new Point3D(p.getX() / mag, p.getY() / mag, p.getZ() / mag);
    }

    private static float mag(Point3D p) {
        return (float)Math.sqrt(p.getX()*p.getX() + p.getY()*p.getY() + p.getZ()*p.getZ());
    }

    public static Point3D cross(Point3D a, Point3D b) {
        return new Point3D(
                a.getY()*b.getZ()-a.getZ()*b.getY(),
                a.getZ()*b.getX()-a.getX()*b.getZ(),
                a.getX()*b.getY()-a.getY()*b.getX()
                );
    }

    public static Point3D mult(Point3D p, float value) {
        return new Point3D(p.getX() * value, p.getY() * value, p.getZ() * value);
    }

    public static Point3D add(Point3D a, Point3D b) {
        return new Point3D(a.getX() + b.getX(), a.getY() + b.getY(), a.getZ() + b.getZ());
    }

    public static Point3D sub(Point3D a, Point3D b) {
        return new Point3D(a.getX() - b.getX(), a.getY() - b.getY(), a.getZ() - b.getZ());
    }

    public static Point3D transform(Point3D point3D, float x, float y, float z) {
        return new Point3D(point3D.getX() + x, point3D.getY() + y, point3D.getZ() + z);
    }

    private MathUtil() {}
}
