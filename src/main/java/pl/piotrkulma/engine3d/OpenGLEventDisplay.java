package pl.piotrkulma.engine3d;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import pl.piotrkulma.engine3d.input.KeyboardInput;
import pl.piotrkulma.engine3d.input.MouseInput;
import pl.piotrkulma.engine3d.model.MapModel;
import pl.piotrkulma.engine3d.model.Point3D;
import pl.piotrkulma.engine3d.model.CameraModel;
import pl.piotrkulma.engine3d.util.MathUtil;
import pl.piotrkulma.engine3d.util.CameraUtil;

public class OpenGLEventDisplay implements GLEventListener {
    private boolean active;
    private GLU glu;
    private Engine3D engine3D;
    private CameraModel cameraModel;
    private MouseInput mouseInput;
    private KeyboardInput keyboardInput;
    private List<Point3D> points;

    public OpenGLEventDisplay(Engine3D engine3D, MouseInput mouseInput, KeyboardInput keyboardInput) {
        this.engine3D = engine3D;
        this.active = true;
        this.glu = new GLU();
        this.mouseInput = mouseInput;
        this.keyboardInput = keyboardInput;
        this.points = new ArrayList<>();
        initScene();
    }

    @Override
    public void init(GLAutoDrawable drawable) {

    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        engine3D.setCursor(!active);
        processKeyboardInput();
        processMouseInput();
        render(drawable);
    }

    private void render(GLAutoDrawable drawable) {
        final GL2 gl = drawable.getGL().getGL2();

        gl.glClear( GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT );
        gl.glLoadIdentity();
        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);

        gl.glColor3f(1f,0f,0f);

        CameraUtil.lookAt(glu, cameraModel);

        if(points != null) {
            gl.glBegin(GL2.GL_QUADS);
            points.stream().forEach(point -> gl.glVertex3f(point.getX(), point.getY(), point.getZ()));
            gl.glEnd();
        }

        gl.glFlush();
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL2 gl = drawable.getGL().getGL2();

        if( height <= 0 )
            height = 1;

        final float h = ( float ) width / ( float ) height;
        gl.glViewport( 0, 0, width, height );
        gl.glMatrixMode( GL2.GL_PROJECTION );
        gl.glLoadIdentity();

        glu.gluPerspective( 45.0f, h, 1.0, 100.0 );
        gl.glMatrixMode( GL2.GL_MODELVIEW );
        gl.glLoadIdentity();
    }

    private void processMouseInput() {
        if(!active) {
            return;
        }

        MouseEvent event = mouseInput.getLastMouseMovedEvent();
        if(mouseInput.getLastMouseMovedEvent() != null) {
            CameraUtil.mouseMove(cameraModel, event.getX(), event.getY());
        }
    }

    private void processKeyboardInput() {
        keyboardInput.poll();

        if(keyboardInput.keyDownOnce(KeyEvent.VK_ESCAPE)) {
            active = !active;
        }

        if(!active) {
            return;
        }

        if(keyboardInput.keyDown(KeyEvent.VK_W)) {
            CameraUtil.moveForward(cameraModel);
        }

        if(keyboardInput.keyDown(KeyEvent.VK_S)) {
            CameraUtil.moveBack(cameraModel);

        }

        if(keyboardInput.keyDown(KeyEvent.VK_D)) {
            CameraUtil.moveRight(cameraModel);
        }

        if(keyboardInput.keyDown(KeyEvent.VK_A)) {
            CameraUtil.moveLeft(cameraModel);
        }
    }

    private void initScene() {
        List<Point3D> basicCube = new ArrayList<>();

        for (int i = 0; i< MathUtil.QUBE.length; i++) {
            basicCube.add(new Point3D(MathUtil.QUBE[i][0], MathUtil.QUBE[i][1], MathUtil.QUBE[i][2]));
        }

        int vz = -5;
        int vx = -5;
        MapModel mapModel = new MapModel();
        for (int i=0; i<mapModel.getMapHeight(); i++) {
            for (int j=0; j<mapModel.getMapWidth(); j++) {
                if(mapModel.getMapAt(i, j) == MapModel.MAP_WALL) {
                    for(Point3D basic : basicCube) {
                        points.add(MathUtil.transform(basic, vx + (j*2), 0, vz + (i* 2)));
                    }
                }
            }
        }

        cameraModel = new CameraModel();
    }
}
