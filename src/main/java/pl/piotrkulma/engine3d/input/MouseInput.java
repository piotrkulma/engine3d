package pl.piotrkulma.engine3d.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseInput implements MouseMotionListener {
    private MouseEvent event;

    public MouseInput() {
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        event = e;
    }

    public MouseEvent getLastMouseMovedEvent() {
        return event;
    }
}
