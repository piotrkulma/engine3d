package pl.piotrkulma.engine3d.input;

public enum KeyState {
    RELEASED,
    PRESSED,
    ONCE
}
